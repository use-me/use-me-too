/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { firebaseConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import { Redirect } from 'react-router-dom';
import '../../index.css';

import { registerAuth } from '../../store/actions/authActions';

class Register extends Component {
  state = {
    username: '',
    email: '',
    password: '',
    firstName: '',
    lastName: '',
  }

  handleChange = (e) => {
    const { target } = e;

    this.setState(state => ({
      ...state,
      [target.id]: target.value,
    }));
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const { props, state } = this;
    const { firebase } = props;
    const newUser = { ...state };

    props.registerAuth(newUser, firebase);
  }

  render() {
    const { auth, authError } = this.props;
    if (auth.uid) {
      return <Redirect to="/feed" />;
    }

    return (
    <div>
      <form onSubmit={this.handleSubmit}>
        <div className='form-row'>
          <div className='form-group col-md-6'>
            <label htmlFor="firstName">First Name</label>
            <input className="form-control form-control-lg" type="text" name="firstName" id="firstName" onChange={this.handleChange} />
          </div>
          <div className='form-group col-md-6'>
            <label htmlFor="lastName">Last Name</label>
            <input className="form-control form-control-lg" type="text" name="lastName" id="lastName" onChange={this.handleChange} />
          </div>
          <hr />
          <div className='form-group col-md-12'>
            <label htmlFor="email">Username</label>
            <input className="form-control form-control-lg" type="username" name="username" id="username" onChange={this.handleChange} />
            <label className='mt-3' htmlFor="email">Email</label>
            <input className="form-control form-control-lg" type="email" name="email" id="email" onChange={this.handleChange} />
            <label className='mt-3' htmlFor="password">Password</label>
            <input className="form-control form-control-lg" type="password" name="password" id="password" onChange={this.handleChange} />
            <button type="submit" className="btn btn-block btn-dark btn-lg mb-2 mt-4">Sign Up</button>
            {authError ? <div className="text-danger center"><p>{authError}</p></div> : null}
          </div>
        </div>
      </form>
    </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.firebase.auth,
  authError: state.auth.authErrorRegister,
});

const mapDispatchToProps = dispatch => ({
  registerAuth: (newUser, firebase) => dispatch(registerAuth(newUser, firebase)),
});

export default compose(
  firebaseConnect(),
  connect(mapStateToProps, mapDispatchToProps),
)(Register);
