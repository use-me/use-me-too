import React from 'react';
import {withRouter} from 'react-router';
import { Link } from 'react-router-dom';
import SignIn from '../../components/auth/SignIn';
import './SignIn.css';

const SignInPage = (props) => {
  return (
    <div className='container'>
      <div className='row mt-4'>
        <div className='col-md-6 offset-md-3 text-center'>
          <h3 className='mt-2 mb-0'><Link to='/' className='logo'>USE ME</Link></h3>
            <span className='mt-0'>Welcome back to the party</span>
              <div className='card text-left mt-4 mb-5'>
                <div className='card-body form-border-top p-4'>
                  <h5 className='mb-3 bold'>Sign In</h5>
                  <p className='mb-4'><small>Don't have an account? <Link to='/register' className='ml-2'> Register <i className='fas fa-caret-right fa-sm ml-0'></i></Link></small></p>
                  <SignIn />
                  <small>By continuing, you agree to Use Me's <Link to='/'>Conditions of Use</Link> and <Link to='/'>Privacy Notice</Link>.</small>
                </div>
              </div>
            </div>
          </div>
        </div>
  )
}

export default withRouter(SignInPage);
