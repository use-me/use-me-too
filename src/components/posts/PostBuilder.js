import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import createPost from '../../store/actions/PostActions';

import Navbar from '../header/Navbar';

class PostBuilder extends Component {
  state = {
    postTitle: '',
    payment: '',
    story: '',
    ask: '',
    email: this.props.email,
  }

  handleChange = (e) => {
    const { target } = e;

    this.setState(state => ({
      ...state,
      [target.id]: target.value,
    }));
  }

  handleSubmit = (e) => {
    e.preventDefault();
    // console.log(this.state);
    const { props, state } = this;
    props.createPost(state);
    console.log(state);
    props.history.push('/feed');
  }

  render() {
    const { auth } = this.props;
    if (!auth.uid) {
      return <Redirect to='/signin' />;
    }

    return (
      <div>
      <Navbar />
        <div className='container'>
          <div className='row mt-5'>
            <div className='col-md-8 offset-md-2'>
              <form onSubmit={this.handleSubmit} className='mb-4 border p-4 white-bg'>
              <h5 className='vollkorn mb-1'>Create a post</h5>
                <p className='text-sm mb-1'>For your own safety/security don't post any personal information.</p>
                <p className='text-sm mb-4'>Responses to your posts are emailed, though the sender won't see your email address until you respond.</p>
                <div className='col-md-8 p-0 pr-2'>
                  <label htmlFor='postTitle'>Post Title</label>
                  <input className='form-control form-control-lg' type='text' maxlength='100' name='postTitle' required id='postTitle' onChange={this.handleChange} />
                </div>
                <div className='col-md-4 p-0 pr-2 mt-4 mb-4'>
                  <label htmlFor='payment'>What would you pay?</label>
                  <input className='form-control form-control-lg' type='number' step='1' pattern='\d+' name='payment' required id='payment' onChange={this.handleChange} />
                  <div><small className='text-muted'>All payments are in $USD</small></div>
                </div>
                  <label htmlFor='story'>Story</label>
                  <textarea name='story' id='story' maxlength='500' cols='10' rows='3' className='form-control form-control-lg' onChange={this.handleChange} />
                  <div className='mb-4'><small className='text-muted'>450 characters or less</small></div>
                  <label htmlFor='ask'>Ask</label>
                  <textarea name='ask' id='ask' maxlength='140' cols='10' rows='3' required className='form-control form-control-lg' onChange={this.handleChange} />
                  <div className='mb-4'><small className='text-muted'>160 characters or less</small></div>
                  <button type='submit' className='btn btn-lg btn-block btn-dark mb-2'>Create Post</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }

const mapStateToProps = state => ({
  auth: state.firebase.auth,
});

const mapDispatchToProps = dispatch => ({
  createPost: post => dispatch(createPost(post)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PostBuilder);
