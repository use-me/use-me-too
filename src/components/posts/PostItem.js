import React from 'react';
import { Link } from 'react-router-dom';

import PostThumb from './PostThumb';

const PostItem = ({ Posts }) => (
  <div>
    {Posts && Posts.map(post => (
      <Link to={`/post/${post.id}`} key={post.id}>
        <PostThumb post={post} />
      </Link>
    ))}
  </div>
);

export default PostItem;
