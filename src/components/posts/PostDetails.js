import React from 'react';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import moment from 'moment';

import Navbar from '../header/Navbar';

const PostDetails = (props) => {
  const { auth } = props;
  if (!auth.uid) {
    return <Redirect to='/signin' />;
  }

  const { post } = props;
  if (post) {
    return (
      <div>
        <Navbar />
        <div className='container mt-3'>
          <div className='row'>
            <div className='col-md-8 offset-md-2'>
              <div className='card mt-4 mb-4'>
                <div className='card-body'>
                  <h4 className='card-title vollkorn mb-3'>{post.postTitle}</h4>
                  <small className='text-muted'>Story</small>
                  <p className='mb-3'>{post.story}</p>
                  <small className='text-muted'>The Ask</small>
                  <p className='mb-4'>{post.ask}</p>
                  <small className='text-muted'>Pay</small>
                  <h3 className='vollkorn mb-2'>${post.payment}</h3>
                  <div className='btn btn-dark mt-4 mb-3'> Private Message </div>
                  <div className='btn btn-outline-danger ml-2 mt-4 mb-3'>Report</div>
                <div>
                <small className='text-muted'>{moment(post.createdAt.toDate()).calendar()} by {post.username}</small>
              </div>
            </div>
          </div>
          <Link className='btn btn-sm btn-light border mb-4' to='/feed'>Back to the feed</Link>
        </div>
      </div>
    </div>
  </div>
    );
  }

  return (
    <div className='container mt-4 text-center'>
      <p>Loading post...</p>
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  const { id } = ownProps.match.params;
  const { Posts } = state.firestore.data;
  const post = Posts ? Posts[id] : null;

  return {
    post,
    auth: state.firebase.auth,
  };
};

export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    { collection: 'Posts' },
  ]),
)(PostDetails);
