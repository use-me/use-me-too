import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';
import { signOut } from '../../store/actions/authActions';

const SignedInLinks = (props) => {
  // As in SignIn.jsx we need to use a function that gets as an argument firebase object
  const handleSignOut = () => {
    const { firebase } = props;
    props.signOut(firebase);
  };

  return (
    <div className='navbar-nav'>
      <Link className='nav-item nav-link ml-4' to="/feed" >Feed</Link>
      <Link className='nav-item nav-link ml-4' to="/" onClick={handleSignOut}>Log Out</Link>
    </div>
  );
};

const mapDispatchToProps = dispatch => ({
  signOut: firebase => dispatch(signOut(firebase)),
});

export default compose(
  firebaseConnect(),
  connect(null, mapDispatchToProps),
)(SignedInLinks);
