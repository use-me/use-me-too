import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';
import SignedInLinks from './SignedInLinks';
import SignedOutLinks from './SignedOutLinks';

const Navbar = (props) => {
  const { auth, profile } = props;
  const links = auth.uid ? <SignedInLinks profile={profile} /> : <SignedOutLinks />;

  return (
    <header className='header'>
      <nav className='navbar navbar-expand-lg navbar-light'>
        <Link className='navbar-brand' to='/'><h3 className='logo mt-2 mb-0'>USE ME</h3><span className='text-sm mt-0'>You're useful to someone</span></Link>
        {links}
      </nav>
    </header>
  );
};

const mapStateToProps = state => ({
  auth: state.firebase.auth,
  profile: state.firebase.profile,
});

export default compose(
  firebaseConnect(),
  connect(mapStateToProps),
)(Navbar);
