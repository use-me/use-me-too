import React from 'react';
import moment from 'moment';

const PostThumb = ({ post }) => (
  <div className='card mb-3'>
    <div className='card-body'>
      <h4 className='card-title text-dark vollkorn'>{post.postTitle}</h4>
        <div className='card-text mt-3'>
          <small className='text-muted'>The Ask</small>
          <p>{post.ask}</p>
          <small className='text-muted mt-3'>Pay</small>
          <h3 className='text-dark vollkorn'>${post.payment}</h3>
        <div className='text-muted mt-2'>
          <small>
            Created {moment(post.createdAt.toDate()).calendar()}
          </small>
        </div>
      </div>
    </div>
  </div>
);

export default PostThumb;
