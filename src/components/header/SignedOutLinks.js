import React from 'react';
import { Link } from 'react-router-dom';

const SignedOutLinks = () => (
  <div className='navbar-nav'>
    <Link className='nav-item nav-link ml-4' to="/register">Register</Link>
    <Link className='nav-item nav-link ml-4' to='/signin'>Sign In</Link>
  </div>
);

export default SignedOutLinks;
