const initState = {
  Posts: [],
};

const resumeReducer = (state = initState, action) => {
  switch (action.type) {
    case 'CREATE_PROJECT':
      console.log('created resume', action.resume);
      return state;
    case 'CREATE_PROJECT_ERROR':
      console.log('resume create error', action.err);
      return state;
    default:
      return state;
  }
};

export default resumeReducer;
