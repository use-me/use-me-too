import React, { Component } from 'react';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import { Redirect, Link } from 'react-router-dom';
import PostItem from '../posts/PostItem';
import Navbar from '../header/Navbar';

class Dashboard extends Component {
  render() {
    // console.log(this.props);
    const { Posts, auth } = this.props;
    if (!auth.uid) {
      return <Redirect to="/signin" />;
    }

    return (
      <div>
      <Navbar />
        <div className='container'>
          <div className='row mt-5'>
            <div className='col-md-6 offset-md-3'>
              <h4 className='vollkorn'>Feed</h4>
              <p className='text-sm'>Ask for anything anonymously <span className='float-right text-muted'>10 mi radius</span></p>
              <hr className='mt-0' />
              <Link className='mb-4 btn btn-dark btn-lg btn-block' to='/post'>
                <small>Create a Post</small>
              </Link>
              <PostItem Posts={Posts} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    Posts: state.firestore.ordered.Posts,
    auth: state.firebase.auth,
  };
};

export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    { collection: 'Posts', orderBy: ['createdAt', 'desc'] },
  ]),
)(Dashboard);
