import React, { Component } from 'react';
import { connect } from 'react-redux';
import { firebaseConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import { Redirect } from 'react-router-dom';

import { signIn } from '../../store/actions/authActions';

class SignIn extends Component {
  state = {
    email: '',
    password: '',
  }

  handleChange = (e) => {
    const { target } = e;

    this.setState(state => ({
      ...state,
      [target.id]: target.value,
    }));
  }

  handleSubmit = (e) => {
    e.preventDefault();

    // As we use react-redux-firebas-v3 we need to pass firebase object to
    // authActions to be authorized by using firebse.auth method
    const { props, state } = this;
    const { firebase } = props;
    const credentials = { ...state };
    const authData = {
      firebase,
      credentials,
    };

    props.signIn(authData);
  }

  render() {
    const { auth, authError } = this.props;
    if (auth.uid) {
      return <Redirect to="/feed" />;
    }

    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label htmlFor="email">Email</label>
          <input className="form-control form-control-lg" type="email" name="email" id="email" onChange={this.handleChange} />
          <label className='mt-3' htmlFor="password">Password</label>
          <input className="form-control form-control-lg" type="password" name="password" id="password" onChange={this.handleChange} />
          <button type="submit" className="btn btn-block btn-dark btn-lg mb-4 mt-4">Login</button>
          {authError ? <div className="text-danger"><p>{authError}</p></div> : null}
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  authError: state.auth.authErrorLogin,
  auth: state.firebase.auth,
});

const mapDispatchToProps = dispatch => ({
  signIn: authData => dispatch(signIn(authData)),
});

// We need firebaseConnect function to provide to this component
// firebase object with auth method.
// You can find more information on the link below
// http://docs.react-redux-firebase.com/history/v3.0.0/docs/auth.html
export default compose(
  firebaseConnect(),
  connect(mapStateToProps, mapDispatchToProps),
)(SignIn);
