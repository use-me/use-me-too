import React from 'react';
import {withRouter} from 'react-router';
import { Link } from 'react-router-dom';
import Register from '../../components/auth/Register';
import './Register.css';

const RegisterPage = (props) => {
  return (
    <div className='container'>
      <div className='row mt-4'>
        <div className='col-md-6 offset-md-3 text-center'>
          <h3 className='mt-2 mb-0'><Link to='/' className='logo'>USE ME</Link></h3>
            <span className='mt-0'>Feel useful, get used</span>
              <div className='card text-left mt-4 mb-5'>
                <div className='card-body form-border-top p-4'>
                  <h5 className='mb-3 bold'>Register</h5>
                  <p className='mb-4'><small>Already have an account? <Link to='/signin' className='ml-2'> Sign-In <i className='fas fa-caret-right fa-sm ml-0'></i></Link></small></p>
                  <Register />
                  <small>By continuing, you agree to Use Me's <Link to='/'>Conditions of Use</Link> and <Link to='/'>Privacy Notice</Link>.</small>
                </div>
              </div>
            </div>
          </div>
        </div>
  )
}

export default withRouter(RegisterPage);
