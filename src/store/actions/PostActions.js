
  const createPost = post => (dispatch, getState, { getFirestore }) => {
  const fireStore = getFirestore();
  const { profile } = getState().firebase;
  const authorId = getState().firebase.auth.uid;

  fireStore.collection('Posts').add({
    ...post,
    username: profile.username,
    email: profile.email,
    authorId,
    createdAt: new Date(),
  }).then(() => dispatch({
    type: 'CREATE_PROJECT',
    post,
  })).catch(err => dispatch({
    type: 'CREATE_PROJECT_ERROR',
    err,
  }));
};

export default createPost;
